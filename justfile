build_musl:
    rustup target add x86_64-unknown-linux-musl
    cargo build --release --target x86_64-unknown-linux-musl

docker_image: build_musl
    docker build -t spaceapi2prometheus -f Containerfile .

podman_image: build_musl
    podman build -t spaceapi2prometheus .

docker_run: docker_image
    docker run -d -p --restart always --name s2p spaceapi2prometheus

podman_run: podman_image
    podman run -d -p --restart-policy=always --name s2p spaceapi2prometheus
