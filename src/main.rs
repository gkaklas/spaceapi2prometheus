mod client;
mod cli;

use std::sync::{Arc, Mutex};
type Body = Arc<Mutex<String>>;

use rocket::{get, launch, routes, State};

#[launch]
fn rocket() -> _ {
    let cli = cli::parse();

    let body: Body = Arc::new(Mutex::new(String::new()));
    let body_state = body.clone();
    std::thread::spawn(move || loop {
        let str = client::get_prometheus_string(cli.url.clone());
        let mut lock = body.lock().unwrap();
        *lock = str;
        drop(lock);

        std::thread::sleep(std::time::Duration::from_millis(cli.delay * 1000));
    });

    rocket::build()
        .manage(body_state)
        .mount("/prometheus", routes![prometheus])
}

#[get("/")]
fn prometheus(body: &State<Body>) -> String {
    format!("{}", body.lock().unwrap())
}
