use clap::Parser;

#[derive(Parser)]
#[command(version, about)]
pub struct Cli {
    /// SpaceAPI endpoint
    #[arg(short, long)]
    pub url: String,

    /// Period of querying, in seconds
    #[arg(short, long, default_value_t = 300)]
    pub delay: u64,
}

pub fn parse() -> Cli {
    Cli::parse()
}
