use regex::Regex;
use spaceapi::Status;

fn get_spaceapi(url: String) -> Status {
    let user_agent = "spaceapi2prometheus";
    let status: Status;
    let client = reqwest::blocking::Client::builder()
        .user_agent(user_agent)
        .build()
        .unwrap()
        .get(url);
    loop {
        let res = client.try_clone().unwrap().send();
        if let Ok(r) = res {
            status = serde_json::from_str(&r.text().unwrap()).unwrap();
            break;
        }
        eprintln!("Failed to connect to endpoint, trying again!");
        std::thread::sleep(std::time::Duration::from_millis(10 * 1000));
    }

    status
}

fn parse_capacity(msg: &str) -> i8 {
    let regex = Regex::new(r"\d*").unwrap();
    let captures = regex.captures(msg).unwrap();

    captures[0].parse::<i8>().unwrap()
}

pub fn get_prometheus_string(url: String) -> String {
    let status = get_spaceapi(url);
    let capacity = parse_capacity(&status.clone().state.unwrap().message.unwrap());
    let lastchange = status.state.unwrap().lastchange.unwrap();

    format!("people {} {}000\n", capacity, lastchange)
}
